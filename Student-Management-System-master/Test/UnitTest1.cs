using NUnit.Framework;
using NUnit.Framework.Constraints;
using Student_Management_System.Models;


namespace Test
{
	public class Tests
	{
		[SetUp]
		public void Setup()
		{
			
		}
		Student stud = new Student();
		[Test]
		public void Test1()
		{
			Assert.That(stud, Is.InstanceOf<Student>());
			Assert.Pass();
		}


		[Test]
		[TestCase("Adam","James", true)]
		[TestCase("kanmoifnanfionieonruiontueElsSelllelmSghhhhhhhhhhhhhhhhhHNHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHAAAA","ey",false)]
		public void FirstName_LastName_CantBeOver64(string name, string Lname, bool expectedResult)
		{
			bool result;
			Student student = new Student();
			student.FirstName = name;
			student.LastName = Lname;
			if (student.FirstName.Length < 65 && student.LastName.Length < 65)
			{
				result = true;
			}
			else
			{
				result = false;
			}
			Assert.AreEqual(expectedResult, result);
		}

		[Test]
		[TestCase("Adam", "James", true)]
		[TestCase(null, null, false)]
		public void FirstName_LastName_CantBeNull(string name, string Lname, bool expectedResult)
		{
			bool result;
			Student student = new Student();
			student.FirstName = name;
			student.LastName = Lname;
			if (student.FirstName != null && student.LastName != null)
			{
				result = true;
			}
			else
			{
				result = false;
			}
			Assert.AreEqual(expectedResult, result);
		}
	}
}